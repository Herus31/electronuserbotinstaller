UB = "  _____ _           _                   "
UB += "| ____| | ___  ___| |_ _ __ ___  _ __  "
UB += "|  _| | |/ _ \/ __| __| '__/ _ \| '_ \ "
UB += "| |___| |  __| (__| |_| | | (_) | | | |"
UB += "|_____|_|\___|\___|\__|_|  \___/|_| |_|"
UB += "                                       "
UB += "Electron UserBot İnstaller"
MESAJ = "Electron UserBot İnstaller Android Kurulum"
MESAJ += "Bizi Tercih Ettiğiniz İçin Teşekkürler"
MESAJ += "İşlem Bitene Kadar Uygulamayı Terk Etmeyin"
YARDIM = "%50, %70 VE %75'te Durakladığında Y Yazıp Enter Yapınız"
bosluk = "\n"
clear
echo -e $UB
echo -e $MESAJ
echo -e $YARDIM
echo " TERMUX GEREKSİNİMLERİNİ GÜNCELLİYORUM "
echo -e $BOSLUK
pkg update -y
clear
echo -e $UB
echo -e $BOSLUK
echo -e $MESAJ
echo -e $BOSLUK
echo " CİHAZINIZA PYTHON KURULUYOR "
echo -e $BOSLUK
pkg install python -y
pip install --upgrade pip
clear
echo -e $UB
echo -e $MESAJ
echo -e $BOSLUK
echo " GIT KURULUYOR "
echo -e $BOSLUK
pkg install git -y
clear
echo -e $SIRI
echo -e $MESAJ
echo -e $BOSLUK
echo " PYROGRAM KURULUYOR "
echo -e $BOSLUK
pip install -U pyrogram
pip install -U pyrogram tgcrypto
clear
echo -e $UB
echo -e $MESAJ
echo -e $BOSLUK
echo " ELECTRONU İNDİRİYORUM "
echo -e $BOSLUK
git clone https://github.com/AnossaTG/ElectronInstaller
clear
echo -e $UB
echo -e $BOSLUK
echo -e $MESAJ
echo -e $BOSLUK
echo " GEREKSİNİMLERİ KURUYORUM..."
echo -e $BOSLUK
cd ElectronInstaller
pip install -r requirements.txt
python -m electroninstaller
